package dev.pablo.academy.service;


import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import dev.pablo.academy.dto.StudentInfoDTO;
import dev.pablo.academy.entity.Course;
import dev.pablo.academy.entity.Student;
import dev.pablo.academy.entity.StudentCourse;
import dev.pablo.academy.repository.CourseRepository;
import dev.pablo.academy.repository.StudentCourseRepository;
import dev.pablo.academy.repository.StudentRepository;
import dev.pablo.academy.service.impl.SubscriptionServiceImpl;

@ExtendWith(SpringExtension.class)
public class SubscriptionServiceTest {

	@InjectMocks
	private SubscriptionServiceImpl subscriptionService;

	@Mock
	private CourseRepository courseRepository;

	@Mock
	private StudentRepository studentRepository;

	@Mock
	private StudentCourseRepository studentCourseRepository;

	@Test
	public void getStudentsByCourseOk() {
		Mockito.when(studentRepository.findActiveStudentsByCourseId(any(), any()))
				.thenReturn(Stream.of(getStudent()).collect(Collectors.toList()));
		List<StudentInfoDTO> students = subscriptionService.getStudentsByCourse(1l, 0, 10);
		assertAll("Student Fields", () -> assertEquals("Pedro", students.get(0).getName()),
				() -> assertEquals("Gonzalez", students.get(0).getSurname()),
				() -> assertEquals(LocalDate.of(1990, 10, 10), students.get(0).getBirthDate()),
				() -> assertEquals("12345678F", students.get(0).getIdNumber()),
				() -> assertTrue(students.get(0).getRegisterDate().isAfter(LocalDateTime.now().minusSeconds(1))));
	}

	@Test
	public void unsubscribeStudentOk() {
		Mockito.when(studentCourseRepository.findByStudentIdAndCourseIdAndLeaveDateIsNull(any(), any()))
				.thenReturn(Optional.ofNullable(getStudentCourse()));
		assertDoesNotThrow(() -> subscriptionService.unsubscribeStudent(1l, 1l));
	}

	@Test
	public void thrownWhenUnSubscribeStudent_StudentEmpty() {
		Mockito.when(studentRepository.findById(any())).thenReturn(Optional.empty());
		Mockito.when(courseRepository.findByIdIn(any()))
				.thenReturn(Stream.of(getCourse()).collect(Collectors.toList()));
		assertThrows(IllegalArgumentException.class, () -> subscriptionService.unsubscribeStudent(1L, 1L));
	}

	private Student getStudent() {
		return Student.builder().birthDate(LocalDate.of(1990, 10, 10)).name("Pedro").surname("Gonzalez").id(1l)
				.idNumber("12345678F").registerDate(LocalDateTime.now()).build();
	}

	private Course getCourse() {
		return Course.builder().id(1l).name("Programación").startDate(LocalDateTime.of(2023, 01, 01, 12, 00))
				.finishDate(LocalDateTime.of(2024, 01, 01, 12, 00)).maxStudents(10).registerDate(LocalDateTime.now())
				.build();
	}

	private StudentCourse getStudentCourse() {
		return StudentCourse.builder().id(1l).registerDate(LocalDateTime.of(2024, 02, 01, 12, 00)).build();
	}
}
