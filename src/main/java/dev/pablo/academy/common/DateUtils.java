package dev.pablo.academy.common;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

public interface DateUtils {

	static final String TIME_ZONE = "Europe/Madrid";

	public static LocalDateTime dateTimeNow() {
		ZoneId timeZone = ZoneId.of(TIME_ZONE);
		return LocalDateTime.now(timeZone);
	}

	public static LocalDate dateNow() {
		ZoneId timeZone = ZoneId.of(TIME_ZONE);
		return LocalDate.now(timeZone);
	}

}
