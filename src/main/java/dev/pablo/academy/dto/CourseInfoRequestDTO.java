package dev.pablo.academy.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseInfoRequestDTO {

	private Long id;

	private String name;

	@JsonProperty("from")
	private LocalDate startDate;

	@JsonProperty("to")
	private LocalDate finishDate;

	private Integer page;

	private Integer size;

}
