package dev.pablo.academy.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateStudentDTO {

	@NotBlank(message = "Name is required")
	@Pattern(regexp = "^\\S.{1,}\\S$", message = "Name is not valid")
	private String name;

	@NotBlank(message = "Surname is required")
	@Pattern(regexp = "^\\S.{0,}\\S$", message = "Surname is not valid")
	private String surname;

	@NotBlank(message = "Id number is required")
	@Pattern(regexp = "^\\S.{1,}\\S$", message = "Id number is not valid")
	private String idNumber;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@NotNull(message = "Birth Date is required")
	private LocalDate birthDate;

}
