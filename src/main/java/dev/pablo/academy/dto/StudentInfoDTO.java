package dev.pablo.academy.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentInfoDTO {

	@Pattern(regexp = "^\\S.{1,}\\S$", message = "Name is not valid")
	private String name;

	@Pattern(regexp = "^\\S.{0,}\\S$", message = "Surname is not valid")
	private String surname;

	@Pattern(regexp = "^\\S.{1,}\\S$", message = "Id number is not valid")
	private String idNumber;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate birthDate;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private LocalDateTime registerDate;

}
