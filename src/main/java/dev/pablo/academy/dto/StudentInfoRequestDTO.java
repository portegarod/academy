package dev.pablo.academy.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentInfoRequestDTO {

	private Long id;

	private String name;

	private LocalDate birthDate;

	private Integer page;

	private Integer size;

}
