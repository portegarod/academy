package dev.pablo.academy.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateCourseDTO {

	@NotBlank(message = "Name is required")
	@Pattern(regexp = "^\\S.{1,}\\S$", message = "Name is not valid")
	private String name;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	@NotNull(message = "Start Date is required")
	private LocalDateTime startDate;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	@NotNull(message = "Finish Date is required")
	private LocalDateTime finishDate;

	@NotNull(message = "Max students are required")
	@Min(value = 1, message = "At least 1 student is required")
	private Integer maxStudents;

}
