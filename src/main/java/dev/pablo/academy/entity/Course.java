package dev.pablo.academy.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "courses")
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true)
	private String name;

	@Column(name = "start_date")
	private LocalDateTime startDate;

	@Column(name = "finish_date")
	private LocalDateTime finishDate;

	@Column(name = "max_students")
	private Integer maxStudents;

	@Column(name = "register_date")
	private LocalDateTime registerDate;

	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
	private List<StudentCourse> studentCourses;

}
