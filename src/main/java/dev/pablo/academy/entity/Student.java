package dev.pablo.academy.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "students")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private String surname;

	@Column(name = "id_number", unique = true)
	private String idNumber;

	@Column(name = "birth_date")
	private LocalDate birthDate;

	@Column(name = "register_date")
	private LocalDateTime registerDate;

	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
	private List<StudentCourse> studentCourses;
}
