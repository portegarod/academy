package dev.pablo.academy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import dev.pablo.academy.entity.Course;

public interface CourseRepository extends JpaRepository<Course, Long>, JpaSpecificationExecutor<Course> {

	Page<Course> findAll(Specification<Course> specification, Pageable pageable);

	@EntityGraph(attributePaths = "studentCourses")
	List<Course> findByIdIn(List<Long> ids);

}
