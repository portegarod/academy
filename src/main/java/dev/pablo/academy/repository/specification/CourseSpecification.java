package dev.pablo.academy.repository.specification;

import java.time.LocalTime;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import dev.pablo.academy.dto.CourseInfoRequestDTO;
import dev.pablo.academy.entity.Course;

public class CourseSpecification {
	public static Specification<Course> filterCourses(CourseInfoRequestDTO courseInfoRequestDTO) {
		return (Root<Course> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {
			Predicate predicate = criteriaBuilder.conjunction();
			if (courseInfoRequestDTO.getId() != null) {
				predicate = criteriaBuilder.and(predicate,
						criteriaBuilder.equal(root.get("id"), courseInfoRequestDTO.getId()));
			}
			if (courseInfoRequestDTO.getName() != null) {
				predicate = criteriaBuilder.and(predicate,
						criteriaBuilder.like(root.get("name"), courseInfoRequestDTO.getName()));
			}
			if (courseInfoRequestDTO.getStartDate() != null) {
				predicate = criteriaBuilder.and(predicate,
						criteriaBuilder.greaterThanOrEqualTo(root.get("startDate"),
								courseInfoRequestDTO.getStartDate().atStartOfDay()));
			}
			if (courseInfoRequestDTO.getFinishDate() != null) {
				predicate = criteriaBuilder.and(predicate,
						criteriaBuilder.lessThanOrEqualTo(root.get("finishDate"),
								courseInfoRequestDTO.getFinishDate().atTime(LocalTime.MAX)));
			}
			return predicate;
		};
	}
}
