package dev.pablo.academy.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import dev.pablo.academy.entity.StudentCourse;

public interface StudentCourseRepository extends JpaRepository<StudentCourse, Long> {

	Optional<StudentCourse> findByStudentIdAndCourseIdAndLeaveDateIsNull(Long studentId, Long courseId);

}
