package dev.pablo.academy.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import dev.pablo.academy.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

	@Query("SELECT s FROM Student s " + "INNER JOIN s.studentCourses sc " + "INNER JOIN sc.course c "
			+ "WHERE c.id = :courseId " + "AND sc.leaveDate IS NULL")
	List<Student> findActiveStudentsByCourseId(Long courseId);

	@Query("SELECT s FROM Student s " + "INNER JOIN s.studentCourses sc " + "INNER JOIN sc.course c "
			+ "WHERE c.id = :courseId " + "AND sc.leaveDate IS NULL")
	List<Student> findActiveStudentsByCourseId(Long courseId, Pageable pageable);

}
