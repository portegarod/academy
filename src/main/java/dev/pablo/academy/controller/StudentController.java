package dev.pablo.academy.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import dev.pablo.academy.dto.CreateStudentDTO;
import dev.pablo.academy.dto.StudentInfoDTO;
import dev.pablo.academy.dto.StudentInfoRequestDTO;
import dev.pablo.academy.service.StudentService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/student-management/students")
@RequiredArgsConstructor
public class StudentController {

	private final StudentService studentService;

	@GetMapping
	public List<StudentInfoDTO> getStudents(@RequestParam(required = false) Long id,
			@RequestParam(required = false) String name,
			@RequestParam(required = false, name = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate birthDate,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
		return studentService
				.getStudents(StudentInfoRequestDTO.builder().name(name).id(id).birthDate(birthDate).page(page)
						.size(size).build());
	}

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public StudentInfoDTO postStudent(@RequestBody @Valid CreateStudentDTO createStudentDTO) {
		return studentService.createStudent(createStudentDTO);
	}

	@PatchMapping("/{id}")
	public StudentInfoDTO patchStudent(@RequestBody @Valid StudentInfoDTO studentInfoDTO, @PathVariable Long id) {
		return studentService.updateStudent(studentInfoDTO, id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteCourse(@PathVariable Long id) {
		studentService.deleteStudent(id);
	}
}
