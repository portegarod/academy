package dev.pablo.academy.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import dev.pablo.academy.dto.CourseInfoDTO;
import dev.pablo.academy.dto.CourseInfoRequestDTO;
import dev.pablo.academy.dto.CreateCourseDTO;
import dev.pablo.academy.service.CourseService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/course-management/courses")
@RequiredArgsConstructor
public class CourseController {

	private final CourseService courseService;

	@GetMapping
	public List<CourseInfoDTO> getCourses(@RequestParam(required = false) Long id,
			@RequestParam(required = false) String name,
			@RequestParam(required = false, name = "from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam(required = false, name = "to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate finishDate,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
		return courseService.getCourses(
				CourseInfoRequestDTO.builder().id(id).name(name).startDate(startDate).finishDate(finishDate).page(page).size(size).build());
	}

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public CourseInfoDTO postCourse(@RequestBody @Valid CreateCourseDTO createCourseDTO) {
		return courseService.createCourse(createCourseDTO);
	}

	@PatchMapping("/{id}")
	public CourseInfoDTO patchCourse(@RequestBody @Valid CourseInfoDTO courseInfoDTO, @PathVariable Long id) {
		return courseService.updateCourse(courseInfoDTO, id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteCourse(@PathVariable Long id) {
		courseService.deleteCourse(id);
	}
}
