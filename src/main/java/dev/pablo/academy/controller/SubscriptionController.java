package dev.pablo.academy.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import dev.pablo.academy.dto.StudentInfoDTO;
import dev.pablo.academy.service.SubscriptionService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/subscription-management")
@RequiredArgsConstructor
public class SubscriptionController {

	private final SubscriptionService subscriptionService;

	@PostMapping("/student/{studentId}/courses")
	@ResponseStatus(value = HttpStatus.CREATED)
	public void subscribeStudentCourse(@PathVariable Long studentId,
			@RequestBody List<Long> courseIds) {
		subscriptionService.subscribeStudent(studentId, courseIds);
	}

	@PatchMapping("/student/{studentId}/courses/{courseId}")
	@ResponseStatus(value = HttpStatus.OK)
	public void unsubscribeStudentCourse(@PathVariable Long studentId,
			@PathVariable Long courseId) {
		subscriptionService.unsubscribeStudent(studentId, courseId);
	}

	@GetMapping("/courses/{courseId}")
	public List<StudentInfoDTO> getSubscribedStudents(@PathVariable Long courseId,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
		return subscriptionService.getStudentsByCourse(courseId, page, size);
	}
}
