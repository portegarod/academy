package dev.pablo.academy.service;

import java.util.List;

import dev.pablo.academy.dto.CourseInfoDTO;
import dev.pablo.academy.dto.CourseInfoRequestDTO;
import dev.pablo.academy.dto.CreateCourseDTO;

public interface CourseService {

	public List<CourseInfoDTO> getCourses(CourseInfoRequestDTO request);

	public CourseInfoDTO createCourse(CreateCourseDTO request);

	public CourseInfoDTO updateCourse(CourseInfoDTO request, Long id);

	public void deleteCourse(Long id);
}
