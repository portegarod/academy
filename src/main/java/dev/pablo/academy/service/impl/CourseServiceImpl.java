package dev.pablo.academy.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.pablo.academy.dto.CourseInfoDTO;
import dev.pablo.academy.dto.CourseInfoRequestDTO;
import dev.pablo.academy.dto.CreateCourseDTO;
import dev.pablo.academy.entity.Course;
import dev.pablo.academy.exception.ContentNotFoundException;
import dev.pablo.academy.mapper.CourseMapper;
import dev.pablo.academy.repository.CourseRepository;
import dev.pablo.academy.repository.StudentRepository;
import dev.pablo.academy.repository.specification.CourseSpecification;
import dev.pablo.academy.service.CourseService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

	private final CourseRepository courseRepository;

	private final StudentRepository studentRepository;

	@Transactional(readOnly = true)
	@Override
	public List<CourseInfoDTO> getCourses(CourseInfoRequestDTO request) {
		Specification<Course> specification = CourseSpecification.filterCourses(request);
		PageRequest pageable = PageRequest.of(request.getPage(), request.getSize());
		List<Course> courses = courseRepository.findAll(specification, pageable).getContent().stream()
				.collect(Collectors.collectingAndThen(Collectors.toList(), Optional::of))
				.filter(list -> !list.isEmpty())
				.orElseThrow(ContentNotFoundException::new);
		return CourseMapper.INSTANCE.courseListToCourseInfoDTOList(courses);
	}

	@Transactional
	@Override
	public CourseInfoDTO createCourse(CreateCourseDTO createCourseDTO) {
		if (createCourseDTO.getStartDate().isAfter(createCourseDTO.getFinishDate())) {
			throw new IllegalArgumentException("Start date must be earlier than finish date");
		}
		Course course = courseRepository.save(CourseMapper.INSTANCE.createCourseDTOToCourse(createCourseDTO));
		return CourseMapper.INSTANCE.courseToCourseInfoDTO(course);
	}

	@Transactional
	@Override
	public CourseInfoDTO updateCourse(CourseInfoDTO request, Long id) {
		Course course = courseRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Course does not exists"));
		Course modifiedCourse = CourseMapper.INSTANCE.courseInfoDTOToCourse(course, request);
		Course updatedCourse = courseRepository.save(modifiedCourse);
		return CourseMapper.INSTANCE.courseToCourseInfoDTO(updatedCourse);
	}

	@Transactional
	@Override
	public void deleteCourse(Long id) {
		Course course = courseRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Course does not exists"));
		studentRepository.findActiveStudentsByCourseId(id).stream().findFirst().ifPresent(student -> {
			throw new IllegalArgumentException("Courses with students cannot be deleted");
		});
		courseRepository.delete(course);
	}

}
