package dev.pablo.academy.service.impl;

import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.pablo.academy.common.DateUtils;
import dev.pablo.academy.dto.CreateStudentDTO;
import dev.pablo.academy.dto.StudentInfoDTO;
import dev.pablo.academy.dto.StudentInfoRequestDTO;
import dev.pablo.academy.entity.Student;
import dev.pablo.academy.exception.ContentNotFoundException;
import dev.pablo.academy.mapper.StudentMapper;
import dev.pablo.academy.repository.StudentRepository;
import dev.pablo.academy.service.StudentService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

	private final StudentRepository studentRepository;

	@Transactional(readOnly = true)
	@Override
	public List<StudentInfoDTO> getStudents(StudentInfoRequestDTO studentInfoRequestDTO) {
		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreNullValues().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
		Example<Student> example = Example
				.of(StudentMapper.INSTANCE.studentInfoRequestDTOToStudent(studentInfoRequestDTO), matcher);
		List<Student> students = studentRepository
				.findAll(example, PageRequest.of(studentInfoRequestDTO.getPage(), studentInfoRequestDTO.getSize()))
				.getContent().stream().collect(Collectors.collectingAndThen(Collectors.toList(), Optional::of))
				.filter(list -> !list.isEmpty()).orElseThrow(() -> new ContentNotFoundException());
		return StudentMapper.INSTANCE.studentListToStudentInfoDTOList(students);
	}

	@Transactional
	@Override
	public StudentInfoDTO createStudent(CreateStudentDTO createStudentDTO) {
		Period period = Period.between(createStudentDTO.getBirthDate(), DateUtils.dateNow());
		if (period.getYears() < 18) {
			throw new IllegalArgumentException("Student needs to be over 18 years old");
		}
		Student student = studentRepository.save(StudentMapper.INSTANCE.createStudentDTOToStudent(createStudentDTO));
		return StudentMapper.INSTANCE.studentToStudentInfoDTO(student);
	}

	@Transactional
	@Override
	public StudentInfoDTO updateStudent(StudentInfoDTO studentInfoDTO, Long id) {
		Student student = studentRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Student does not exists"));
		Student modifiedStudent = StudentMapper.INSTANCE.studentInfoDTOToStudent(student, studentInfoDTO);
		Student updatedStudent = studentRepository.save(modifiedStudent);
		return StudentMapper.INSTANCE.studentToStudentInfoDTO(updatedStudent);
	}

	@Transactional
	@Override
	public void deleteStudent(Long id) {
		Student student = studentRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Student does not exists"));
		studentRepository.delete(student);
	}

}
