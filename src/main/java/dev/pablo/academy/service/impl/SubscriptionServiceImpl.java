package dev.pablo.academy.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.pablo.academy.common.DateUtils;
import dev.pablo.academy.dto.StudentInfoDTO;
import dev.pablo.academy.entity.Course;
import dev.pablo.academy.entity.Student;
import dev.pablo.academy.entity.StudentCourse;
import dev.pablo.academy.exception.ContentNotFoundException;
import dev.pablo.academy.mapper.SubscriptionMapper;
import dev.pablo.academy.repository.CourseRepository;
import dev.pablo.academy.repository.StudentCourseRepository;
import dev.pablo.academy.repository.StudentRepository;
import dev.pablo.academy.service.SubscriptionService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SubscriptionServiceImpl implements SubscriptionService {

	private final CourseRepository courseRepository;

	private final StudentRepository studentRepository;

	private final StudentCourseRepository studentCourseRepository;

	@Transactional
	@Override
	public void subscribeStudent(Long studentId, List<Long> courseIds) {
		Student student = studentRepository.findById(studentId)
				.orElseThrow(() -> new IllegalArgumentException("Student does not exists"));
		List<Course> courses = courseRepository.findByIdIn(courseIds).stream()
				.collect(Collectors.collectingAndThen(Collectors.toList(), Optional::of))
				.filter(list -> !list.isEmpty())
				.orElseThrow(() -> new IllegalArgumentException("Courses do not exist"));
		for (Course course : courses) {
			validateMaxStudents(course);
			validateStudentSubscription(student, course);
			studentCourseRepository.save(SubscriptionMapper.INSTANCE.createStudentCourse(course, student));
		}
	}

	private void validateStudentSubscription(Student student, Course course) {
		course.getStudentCourses().stream()
				.filter(studentCourse -> studentCourse.getLeaveDate() == null
						&& student.getId().equals(studentCourse.getStudent().getId()))
				.findFirst().ifPresent(studentCourse -> {
					throw new IllegalArgumentException("Student already subscribed to " + course.getName());
				});
	}

	private void validateMaxStudents(Course course) {
		long activeStudents = course.getStudentCourses().stream()
				.filter(studentCourse -> studentCourse.getLeaveDate() == null).count();
		if (activeStudents > course.getMaxStudents()) {
			throw new IllegalArgumentException("Max students reached in " + course.getName());
		}
	}

	@Transactional
	@Override
	public void unsubscribeStudent(Long studentId, Long courseId) {
		StudentCourse studentCourse = studentCourseRepository
				.findByStudentIdAndCourseIdAndLeaveDateIsNull(studentId, courseId)
				.orElseThrow(() -> new IllegalArgumentException("Student is not subscribed to the course"));
		studentCourse.setLeaveDate(DateUtils.dateTimeNow());
		studentCourseRepository.save(studentCourse);
	}

	@Transactional(readOnly = true)
	@Override
	public List<StudentInfoDTO> getStudentsByCourse(Long courseId, Integer page, Integer size) {
		List<Student> students = studentRepository.findActiveStudentsByCourseId(courseId, PageRequest.of(page, size))
				.stream()
				.collect(Collectors.collectingAndThen(Collectors.toList(), Optional::of))
				.filter(list -> !list.isEmpty()).orElseThrow(ContentNotFoundException::new);
		return SubscriptionMapper.INSTANCE.studentListToStudentInfoDTOList(students);
	}

}
