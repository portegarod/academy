package dev.pablo.academy.service;

import java.util.List;

import dev.pablo.academy.dto.CreateStudentDTO;
import dev.pablo.academy.dto.StudentInfoDTO;
import dev.pablo.academy.dto.StudentInfoRequestDTO;

public interface StudentService {

	public List<StudentInfoDTO> getStudents(StudentInfoRequestDTO studentInfoRequestDTO);

	public StudentInfoDTO createStudent(CreateStudentDTO createStudentDTO);

	public StudentInfoDTO updateStudent(StudentInfoDTO studentInfoDTO, Long id);

	public void deleteStudent(Long id);

}
