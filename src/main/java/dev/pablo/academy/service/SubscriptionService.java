package dev.pablo.academy.service;

import java.util.List;

import dev.pablo.academy.dto.StudentInfoDTO;

public interface SubscriptionService {

	public void subscribeStudent(Long studentId, List<Long> courseIds);

	public void unsubscribeStudent(Long studentId, Long courseId);

	public List<StudentInfoDTO> getStudentsByCourse(Long courseId, Integer page, Integer size);

}
