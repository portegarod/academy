package dev.pablo.academy.exception;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 9092704620253209307L;

	public NotFoundException() {
		super();
	}

}
