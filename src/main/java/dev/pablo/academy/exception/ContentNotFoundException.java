package dev.pablo.academy.exception;

public class ContentNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -8780639322953707146L;

	public ContentNotFoundException() {
		super();
	}

}
