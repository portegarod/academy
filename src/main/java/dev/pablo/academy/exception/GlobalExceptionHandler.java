package dev.pablo.academy.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response> handleGlobalException(Exception e) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Response.builder()
				.message("Internal Server Error").status(HttpStatus.INTERNAL_SERVER_ERROR.value()).build());
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<Response> handleRuntimeException(RuntimeException e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(Response.builder().message(e.getMessage()).status(HttpStatus.BAD_REQUEST.value()).build());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<List<ValidationError>> handleMethodArgumentNotValidException(
			MethodArgumentNotValidException ex) {
		List<ValidationError> validationErrors = ex.getBindingResult().getFieldErrors().stream().map(
				error -> ValidationError.builder().message(error.getDefaultMessage()).field(error.getField()).build())
				.collect(Collectors.toList());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationErrors);
	}

	@ExceptionHandler(ContentNotFoundException.class)
	public ResponseEntity<Void> handleContentNotFoundException(ContentNotFoundException e) {
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Void> handleNotFoundException(NotFoundException e) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ResponseEntity<Void> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
	}
}
