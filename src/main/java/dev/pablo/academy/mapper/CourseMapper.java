package dev.pablo.academy.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import dev.pablo.academy.common.DateUtils;
import dev.pablo.academy.dto.CourseInfoDTO;
import dev.pablo.academy.dto.CreateCourseDTO;
import dev.pablo.academy.entity.Course;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, imports = DateUtils.class, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface CourseMapper {

	CourseMapper INSTANCE = Mappers.getMapper(CourseMapper.class);

	List<CourseInfoDTO> courseListToCourseInfoDTOList(List<Course> courses);

	@Mapping(target = "registerDate", expression = "java(DateUtils.dateTimeNow())")
	Course createCourseDTOToCourse(CreateCourseDTO createCourseDTO);

	CourseInfoDTO courseToCourseInfoDTO(Course course);

	Course courseInfoDTOToCourse(@MappingTarget Course course, CourseInfoDTO courseInfoDTO);

}
