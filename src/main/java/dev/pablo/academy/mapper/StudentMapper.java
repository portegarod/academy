package dev.pablo.academy.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import dev.pablo.academy.common.DateUtils;
import dev.pablo.academy.dto.CreateStudentDTO;
import dev.pablo.academy.dto.StudentInfoDTO;
import dev.pablo.academy.dto.StudentInfoRequestDTO;
import dev.pablo.academy.entity.Student;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, imports = DateUtils.class, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface StudentMapper {

	StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

	List<StudentInfoDTO> studentListToStudentInfoDTOList(List<Student> students);

	@Mapping(target = "registerDate", expression = "java(DateUtils.dateTimeNow())")
	Student createStudentDTOToStudent(CreateStudentDTO createStudentDTO);

	StudentInfoDTO studentToStudentInfoDTO(Student student);

	Student studentInfoDTOToStudent(@MappingTarget Student student, StudentInfoDTO studentInfoDTO);
	
	Student studentInfoRequestDTOToStudent(StudentInfoRequestDTO studentInfoRequestDTO);


}
