package dev.pablo.academy.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import dev.pablo.academy.common.DateUtils;
import dev.pablo.academy.dto.StudentInfoDTO;
import dev.pablo.academy.entity.Course;
import dev.pablo.academy.entity.Student;
import dev.pablo.academy.entity.StudentCourse;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT, imports = DateUtils.class)
public interface SubscriptionMapper {

	SubscriptionMapper INSTANCE = Mappers.getMapper(SubscriptionMapper.class);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "registerDate", expression = "java(DateUtils.dateTimeNow())")
	StudentCourse createStudentCourse(Course course, Student student);

	List<StudentInfoDTO> studentListToStudentInfoDTOList(List<Student> students);

}
