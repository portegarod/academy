INSERT INTO courses (name, start_date, finish_date, max_students, register_date)
VALUES ('Curso de Programación', '2023-07-22 10:00:00', '2023-08-30 18:00:00', 30, '2023-07-21 15:30:00');
INSERT INTO courses (name, start_date, finish_date, max_students, register_date)
VALUES ('Jardinería Básica', '2023-08-10 09:30:00', '2023-09-20 13:00:00', 25, '2023-07-22 16:45:00');
INSERT INTO courses (name, start_date, finish_date, max_students, register_date)
VALUES ('Introducción a la Carpintería', '2023-08-15 14:00:00', '2023-09-25 17:30:00', 20, '2023-07-23 17:15:00');
INSERT INTO students (name, surname, id_number, birth_date, register_date) 
VALUES ('John', 'Doe', '123456789', '1990-01-15', '2023-07-23 10:30:00');
INSERT INTO students (name, surname, id_number, birth_date, register_date) 
VALUES ('Alice', 'Smith', '987654321', '1985-05-20', '2023-07-23 15:45:00');
INSERT INTO student_courses (register_date, leave_date, course_id, student_id)
VALUES ('2023-07-23 10:30:00','2023-07-24 10:30:00', 1, 1);
INSERT INTO student_courses (register_date, course_id, student_id)
VALUES ('2023-07-23 10:30:00', 2, 1);

